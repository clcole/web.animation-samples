const cube = document.querySelector(".cube");
const radioGroup = document.querySelector(".radio-group");
let currentSide = "";

function changeSide() {
  const checkedRadio = radioGroup.querySelector(":checked");
  const newSide = `show-${checkedRadio.value}`;

  if(currentSide) {
    cube.classList.remove(currentSide);
  }

  cube.classList.add(newSide);
  currentSide = newSide;
}

//set initial side
changeSide();

radioGroup.addEventListener("change", changeSide);

const carousel = document.querySelector(".carousel");
const carouselFaces = document.querySelectorAll(".carousel__face");
const carouselRectObj = carousel.getBoundingClientRect();
const carouselWidth = carouselRectObj.width;
const carouselHeight = carouselRectObj.height;

const rangeOptionFaces = document.querySelector(".range-option__faces");
const rangeOptionOutput = document.querySelector(".range-option__output");
const radioOption = document.querySelector(".radio-option");
const buttonPrev = document.querySelector(".button-option__prev");
const buttonNext = document.querySelector(".button-option__next");

let isHorizontal = true;
let rotateFunction = "rotateY";
let rotateIndex = 0; // prev/next button counter
let theta; // the rotation angle in radians
let radius; // the distant to push each face out from the carousel

function rotateCarousel() {
  // multiple by -1 to rotateY left on next or rotateX down on next
  carousel.style.transform = `translateZ(${-radius}px) ${rotateFunction}(${theta * rotateIndex * -1}rad)`;
}

function changeCarousel() {
  const faceSize = isHorizontal ? carouselWidth : carouselHeight;
  const faceCount = rangeOptionFaces.value;
  const colorOffset = 360 / faceCount;
  
  // 360 degrees divided by the number of carousel faces
  // 2 pi radians = 360 degrees
  // pi radians = 180 degrees
  theta = (2 * Math.PI) / faceCount;
  
  // tan(radians) = opposite side/adjacent side => adjacent side = opposite side/tan(radians)
  radius = (faceSize / 2) / Math.tan(theta / 2);

  for(let i = 0; i < carouselFaces.length; i++) {
    let face = carouselFaces[i];

    // visible face
    if(i < faceCount) {
      face.style.opacity = 1;
      face.style.backgroundColor = `hsla(${colorOffset*i}, 100%, 50%, 0.9)`;
      face.style.transform = `${rotateFunction}(${theta * i}rad) translateZ(${radius}px)`;
    }
    // hidden face
    else {
      face.style.opacity = 0;
      face.style.transform = "none";
    }
  }

  rotateCarousel();
}

function changeOrientation() {
  const checkedRadio = radioOption.querySelector(":checked");
  isHorizontal = checkedRadio.value === "horizontal";
  rotateFunction = isHorizontal ? "rotateY" : "rotateX";
  changeCarousel();
}

function setRangeOutput() {
  rangeOptionOutput.value = rangeOptionFaces.value; 
}

rangeOptionFaces.addEventListener("input", setRangeOutput);
rangeOptionFaces.addEventListener("input", changeCarousel);
radioOption.addEventListener("change", changeOrientation);

buttonPrev.addEventListener("click", () => {
  rotateIndex--;
  rotateCarousel();
});

buttonNext.addEventListener("click", () => {
  rotateIndex++;
  rotateCarousel();
});

document.addEventListener("DOMContentLoaded", changeOrientation);
document.addEventListener("DOMContentLoaded", setRangeOutput);

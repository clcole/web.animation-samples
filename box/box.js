const box = document.querySelector(".box");
const radioGroup = document.querySelector(".radio-group");
let currentSide = "";

function changeSide() {
  const checkedRadio = radioGroup.querySelector(":checked");
  const newSide = `show-${checkedRadio.value}`;

  if(currentSide) {
    box.classList.remove(currentSide);
  }

  box.classList.add(newSide);
  currentSide = newSide;
}

//set initial side
changeSide();

radioGroup.addEventListener("change", changeSide);